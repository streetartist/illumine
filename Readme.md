# Illumine
## Use Illumine to build any website.

# 文档
https://github.com/streetartist/Illumine/wiki

# 我们的想法
Python中一直没有像Wordpress这样的网站框架，所以我DIY了一个

目标是让用户在不修改内核代码的情况下，通过编写插件电池主题，来编写网站

创意：电池是事先编写好的网页框架（框架中的框架），加快编写速度

# 代码正在上传……

# 欢迎大家为Illumine编写插件电池主题！

# 版本
- 0.2.0
由于Github网络问题，没能上传
实现Part和Tool功能

- 0.0.2
- - 非常困难，在路径中绕来绕去，终于完成了
- - 实现了完整的batteries、theme、plugins
- - 以及设置文档conf.py
- - 暂时不写文档，可以看看示例代码

- 0.0.1
- - 原形构建
- - 实现url_to函数
